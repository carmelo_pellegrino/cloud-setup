#!/bin/bash

if [ "${UID}" = "0" ]; then
  ## ROOT
  # First of all
  passwd
  useradd budda
  passwd budda
  usermod budda -a -G wheel,users,video,audio,root
  visudo
  hostnamectl set-hostname meloness

  # Install software
  yum remove -y lvm2
  yum install -y docker wget git vim nmap-ncat tmux epel-release net-tools bash-completion
  yum install -y bash-completion bash-completion-extras
  yum install -y htop
  curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh >gitlab.sh
  chmod +x gitlab.sh
  ./gitlab.sh
  yum install -y gitlab-ci-multi-runner
  systemctl daemon-reload
  systemctl start docker
  systemctl enable docker
else
  ## USER
  ssh-keygen
  cat .ssh/id_rsa.pub
  # Installation of the user environment
  git clone git@bitbucket.org:carmelo_pellegrino/home.git .home
  mv .bashrc .bashrc.stock
  mv .bash_profile .bash_profile.stock
  ln -s `pwd`/.home/.bash* .
  ln -s `pwd`/.home/.inputrc .
  ln -s `pwd`/.home/.vimrc .
  ln -s `pwd`/.home/.vim .
  ln -s `pwd`/.home/.melo .
  ln -s `pwd`/.home/.tmux.conf .
fi
